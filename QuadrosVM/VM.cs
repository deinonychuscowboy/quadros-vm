﻿using QuadrosLIS.Model;
using QuadrosLIS.Test;

namespace QuadrosVM;

internal class VM {
    public static void Main(string[] args) {
        BitTest.test();
        TritTest.test();
        QuadritTest.test();
        Console.WriteLine("Tests complete!");
        
        Link<Bit> out1 = Link<Bit>.new_terminal();
        AndGate<Bit> gate1 = new(2, new[]{out1});
        Link<Bit> in1 = new(gate1, 0);
        Link<Bit> in2 = new(gate1, 1);
        
        in1.output(Bit.T);
        in2.output(Bit.T);
        Console.WriteLine(out1);
        in2.output(Bit.F);
        Console.WriteLine(out1);

        Link<Quadrit> out2 = Link<Quadrit>.new_terminal();
        AndGate<Quadrit> gate2 = new(2, new[] { out2 });
        Link<Quadrit> in3 = new(gate2, 0);
        Link<Quadrit> in4 = new(gate2, 1);
        
        in3.output(Quadrit.B);
        in4.output(Quadrit.B);
        Console.WriteLine(out2);
        in4.output(Quadrit.G);
        Console.WriteLine(out2);
        in3.output(Quadrit.Y);
        Console.WriteLine(out2);
        in3.output(Quadrit.R);
        Console.WriteLine(out2);
    }
}