using System.Collections;

namespace QuadrosLIS.Util; 

/// Allows the built-in C# operator .. to work in foreach loops
public static class RangeEx
{
    public static RangeEnumerator GetEnumerator(this Range range)
    {
        if (range.Start.IsFromEnd || range.End.IsFromEnd)
        {
            throw new ArgumentException(nameof(range));
        }

        return new RangeEnumerator(range.Start.Value, range.End.Value);
    }

    public struct RangeEnumerator : IEnumerator<int>
    {
        private readonly int _end;
        private int _current;

        public RangeEnumerator(int start, int end)
        {
            this._current = start - 1;
            this._end = end;
        }

        public int Current => this._current;
        object IEnumerator.Current => this.Current;

        public bool MoveNext() => ++this._current < this._end;

        public void Dispose() { }
        public void Reset()
        {
            throw new NotImplementedException();
        }
    }
}