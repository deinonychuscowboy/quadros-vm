namespace QuadrosLIS.Model;
#pragma warning disable CS8509
// ReSharper disable ArrangeStaticMemberQualifier

public interface Signal<T> {
	static abstract T Low {get;}
	static abstract T High {get;}
    bool IsTruthy {get;}
    bool IsKnown {get;}
}

public interface Trinator {
    Trit Tri {get;}
}

public interface Binator {
    Bit Bi {get;}
}

public interface BoolOps<T>{
	// boolean operations have a lot of synonyms, particularly within related realms that are mathematically identical but used for different purposes, such as formal logic
	// the operations here generally favor the names typically used in transistors and circuit diagrams, and borrow from logic for a few operations that are not commonly used in electronics
	// xnor/nxor is also called by one of its logic names, equivalence, because I don't want to get into the xnor/nxor debate.

	// one-argument operators (4 total)
	// 2 constant and 1 identity operators omitted (tautology, contradiction, proposition operators in formal logic)
	
	T not();
	void not(out T result);

	// two-argument operators (16 total)
	// 2 constant, 2 identity, and 2 negated identity operators omitted (tautology, contradiction, proposition P, propsition Q, not P, and not Q operators in formal logic)
	// corner-type operators (Unequivocally True in exactly one position at the corner of the chart)
	
	 // also called conjunction
	T and(in T other);
	void and(in T other, out T result);

	 // also called joint denial
	T nor(in T other);
	void nor(in T other, out T result);

	 // nonimplication, also called greater than
	T nimp(in T other);
	void nimp(in T other, out T result);

	 // converse nonimplication, also called less than
	T ncnv(in T other);
	void ncnv(in T other, out T result);

	// spreading-type operators (Unequivocally False in exactly one position at the corner of the chart)
	
	 // inclusive or, also called disjunction
	T or(in T other);
	void or(in T other, out T result);

	 // nand, also called alternative denial
	T nand(in T other);
	void nand(in T other, out T result);

	 // implication, also called greater than or equal, material conditional
	T imp(in T other);
	void imp(in T other, out T result);

	 // converse implication, also called less than or equal to
	T cnv(in T other);
	void cnv(in T other, out T result);

	// centralized operators (symmetric tables)
	
	 // exclusive or, also called exclusive disjunction, not equal to
	T xor(in T other);
	void xor(in T other, out T result);

	 // equivalence, also called xnor, nxor, biconditional, equal to
	T eqv(in T other);
	void eqv(in T other, out T result);

}

public interface TernOps<T>{
	// These are operations which are not sensical in a 2-state system or collapse to the same 2-state form as a boolean operation
	// There are 27 one-argument operators and 19683 two-argument operators in ternary, so obviously only the particularly useful ones are represented here

	// one-argument operators

	T known();
	void known(out T result);

	T possible();
	void possible(out T result);

	T necessary();
	void necessary(out T result);

	// two-argument operators
	// eqivalence related flattening operators - similar to eqv but take harder view of U, saying explicitly that it is either both true and false or neither true nor false.
	// these operators always return T or F, never U
	// corner cases (where neither arg is U) behave identically to eqv
	// symmetric operators (figure 8 shaped tables)
	// over is a bit trickier to understand than under because both are symmetric. So, T OVER U is T, even though taking the related Priest-y definition of U as "both true and false", in words this reads like "true is both true and false".
	// it is more appropriate to say that OVER and UNDER behave like some concept between "is" and "contains", perhaps like "of the same substance. True is the same stuff as both, and both is the same stuff as true, but true and false are not of the same stuff, in OVER's understanding.
	// UNDER is the opposite; taking Kleene's definition of U as "neither true nor false", True is not of the same stuff as neither, and neither is not of the same stuff as true.
	
	 // overdetermination operator - takes a Priest-like view of U, assuming it is both true and false at the same time
	T over(in T other);
	void over(in T other, out T result);

	 // underdetermination operator - takes a Kleene-like view of U, assuming it is neither true nor false
	T under(in T other);
	void under(in T other, out T result);

	// these two are related to over and under, being the non-symmetric versions (s-shaped tables)
	
	 // related to over - provides the expected "both is true but true is not both" behavior discussed above
	T satisfies(in T other);
	void satisfies(in T other, out T result);

	 // related to under - states that while true is true, and true may be a "real value" hidden behind unknown, unknown cannot be the "real value" for anything.
	T provides(in T other);
	void provides(in T other, out T result);

}

public interface LucOps<T>{
	// Łukasiewicz-type expansion of two-argument BoolOps at >2 states
	// Łukasiewicz defined this only for implication (I think), but the difference between Kleene/Priest implication and Łukasiewicz implication is translatable to other boolean operations.
	// the Kleene/Priest (here considered "normal") behavior and the Łukasiewicz behavior are identical for extreme values, but differ in how they handle middle regions
	// at high state levels, including quaternary and higher systems, Kleene/Priest expansion causes the truth table to make a repeating L shape, while Łukasiewicz expansion causes a large triangular section and repeating diagonal lines.
	// put another way, Kleene/Priest expansion is parallel to the axis of symmetry, whereas Łukasiewicz is perpendicular to it.
	// corner-type operators (Unequivocally True in exactly one position at the corner of the chart)

	T land(in T other);
	void land(in T other, out T result);

	T lnor(in T other);
	void lnor(in T other, out T result);

	T lnimp(in T other);
	void lnimp(in T other, out T result);

	T lncnv(in T other);
	void lncnv(in T other, out T result);

	// spreading-type operators (Unequivocally False in exactly one position at the corner of the chart)

	T lor(in T other);
	void lor(in T other, out T result);

	T lnand(in T other);
	void lnand(in T other, out T result);

	T limp(in T other);
	void limp(in T other, out T result);

	T lcnv(in T other);
	void lcnv(in T other, out T result);

	// centralized operators (symmetric tables)
	// to understand why Łukasiewicz affects these two, remember that the literal definition of how Łukasiewicz implication differs from Kleene/Priest is that U -> U = T (for k/p, U -> U = U)
	// so, it stands to reason that, for these operations, U XORL U = F and U EQVL U = T, contrary to k/p (where both are U)
	// XORL and EQVL, here, say that since we don't know what either value is, they are "effectively the same" in that they are unknowable.

	T lxor(in T other);
	void lxor(in T other, out T result);

	T leqv(in T other);
	void leqv(in T other, out T result);

}

public interface QuadOps<T>{
	// one-argument operators
	
	 // cause values to collapse to their affinities, i.e. unknowns shuffle out into their affiliated knowns. This function could also be called truthy(), as it answers the truthiness question directly.
	T affinity();
	void affinity(out T result);

}

// note Trit cannot implement Binator, because there is no sensible way to translate U
// "going up" also does not make sense; you cannot tell if a 2:F should be a 4:R or 4:Y, and making a choice is not safe without more context.

public readonly struct Quadrit:Signal<Quadrit>,Trinator,Binator,BoolOps<Quadrit>,TernOps<Quadrit>,LucOps<Quadrit>,QuadOps<Quadrit> {
    public Quadrit(byte val) {
	    this.val = val;
    }
    public const byte R_ = 0b01;
    public const byte Y_ = 0b00;
    public const byte G_ = 0b10;
    public const byte B_ = 0b11;
    public static readonly Quadrit R = R_;
    public static readonly Quadrit Y = Y_;
    public static readonly Quadrit G = G_;
    public static readonly Quadrit B = B_;
    
    public static implicit operator byte(Quadrit input) => input.val;
    public static implicit operator Quadrit(byte input) => new(input);

    private readonly byte val;

    public static Quadrit Low => Quadrit.R_;
    public static Quadrit High => Quadrit.B_;

    public bool IsTruthy => (this.val & 0b10) > 0b0;
    public bool IsKnown => (this.val & 0b01) > 0b0;

    public Trit Tri => this.IsTruthy && this.IsKnown ? Trit.T : !this.IsTruthy && this.IsKnown ? Trit.F : Trit.U;

    public Bit Bi => this.IsTruthy ? Bit.T : Bit.F;

    public override string ToString() {
	    return (byte)this switch {Quadrit.R_=>"RED",Quadrit.Y_=>"YELLOW",Quadrit.G_=>"GREEN",Quadrit.B_=>"BLUE"};
    }

    public Quadrit and(in Quadrit other) {
	    this.and(in other, out Quadrit result);
	    return result;
    }
    public void and(in Quadrit other, out Quadrit result) {
	    result=this.val switch {
		    Quadrit.R_ => Quadrit.R_,
		    Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.R_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.R_, Quadrit.Y_ => Quadrit.Y_, _ => Quadrit.G_ },
		    Quadrit.B_ => other
	    };
    }

    public Quadrit or(in Quadrit other) {
	    this.or(in other, out Quadrit result);
	    return result;
    }
    public void or(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other,
		    Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.B_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.B_, _ => Quadrit.G_ },
		    Quadrit.B_ => Quadrit.B_
	    };
    }

    public Quadrit not() {
	    this.not(out Quadrit result);
	    return result;
    }
    public void not(out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => Quadrit.B_,
		    Quadrit.Y_ => Quadrit.G_,
		    Quadrit.G_ => Quadrit.Y_,
		    Quadrit.B_ => Quadrit.R_
	    };
    }

    public Quadrit nand(in Quadrit other) {
	    this.nand(in other, out Quadrit result);
	    return result;
    }
    public void nand(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => Quadrit.B_,
		    Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.B_, _ => Quadrit.G_ },
		    Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.B_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.B_ => other.not()
	    };
    }

    public Quadrit nor(in Quadrit other) {
	    this.nor(in other, out Quadrit result);
	    return result;
    }
    public void nor(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other.not(),
		    Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.R_, Quadrit.G_ => Quadrit.Y_, _ => Quadrit.G_ },
		    Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.R_, _ => Quadrit.Y_ },
		    Quadrit.B_ => Quadrit.R_
	    };
    }

    public Quadrit xor(in Quadrit other) {
		this.xor(in other, out Quadrit result);
		return result;
	}
	public void xor(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other,
		    Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.G_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.B_ => other.not()
	    };
    }

    public Quadrit eqv(in Quadrit other) {
		this.eqv(in other, out Quadrit result);
		return result;
	}
	public void eqv(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other.not(),
		    Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.G_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.B_ => other
	    };
    }
    public Quadrit ncnv(in Quadrit other) {
		this.ncnv(in other, out Quadrit result);
		return result;
	}
	public void ncnv(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other,
		    Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.R_, Quadrit.Y_ => Quadrit.Y_, _ => Quadrit.G_ },
		    Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.R_, _ => Quadrit.Y_ },
		    Quadrit.B_ => Quadrit.R_
	    };
    }

    public Quadrit nimp(in Quadrit other) {
		this.nimp(in other, out Quadrit result);
		return result;
	}
	public void nimp(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => Quadrit.R_,
		    Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.R_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.R_, Quadrit.G_ => Quadrit.Y_, _ => Quadrit.G_ },
		    Quadrit.B_ => other.not()
	    };
    }

    public Quadrit cnv(in Quadrit other) {
		this.cnv(in other, out Quadrit result);
		return result;
	}
	public void cnv(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => other.not(),
		    Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.B_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.B_, _ => Quadrit.G_ },
		    Quadrit.B_ => Quadrit.B_
	    };
    }

    public Quadrit imp(in Quadrit other) {
		this.imp(in other, out Quadrit result);
		return result;
	}
	public void imp(in Quadrit other, out Quadrit result) {
	    result = this.val switch {
		    Quadrit.R_ => Quadrit.B_,
		    Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.B_, _ => Quadrit.G_ },
		    Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.B_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.Y_ },
		    Quadrit.B_ => other
	    };
    }
	
	public Quadrit known() {
		this.known(out Quadrit result);
		return result;
	}
	public void known(out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.B_,
			Quadrit.Y_ => Quadrit.R_,
			Quadrit.G_ => Quadrit.R_,
			Quadrit.B_ => Quadrit.B_
		};
	}

	public Quadrit possible() {
		this.possible(out Quadrit result);
		return result;
	}
	public void possible(out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.R_,
			Quadrit.Y_ => Quadrit.B_,
			Quadrit.G_ => Quadrit.B_,
			Quadrit.B_ => Quadrit.B_
		};
	}

	public Quadrit necessary() {
		this.necessary(out Quadrit result);
		return result;
	}
	public void necessary(out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.R_,
			Quadrit.Y_ => Quadrit.R_,
			Quadrit.G_ => Quadrit.R_,
			Quadrit.B_ => Quadrit.B_
		};
	}

	// unlike the 3-state implementations, which always return a known value, these always return a "truthy" or "falsy" value, which may or may not be known,
	// and provide additional useful information about the involvement of unknowns in the operation.
	// the rule for 3-state, that e.g. UNDER is only true in exactly two positions, becomes that UNDER is only truthy in exactly two positions, and additionally that it is only
	// unequivocally true or false if no unknown values were involved in the operation (if any were, it is a falsy unknown aka Y)
	public Quadrit over(in Quadrit other) {
		this.over(in other, out Quadrit result);
		return result;
	}
	public void over(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.val switch { Quadrit.B_ => Quadrit.R_, Quadrit.R_ => Quadrit.B_, _ => Quadrit.G_ },
			Quadrit.Y_ => Quadrit.G_,
			Quadrit.G_ => Quadrit.G_,
			Quadrit.B_ => other.val switch { Quadrit.R_ => Quadrit.R_, Quadrit.B_ => Quadrit.B_, _ => Quadrit.G_ }
		};
	}

	public Quadrit under(in Quadrit other) {
		this.under(in other, out Quadrit result);
		return result;
	}
	public void under(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.val switch { Quadrit.R_ => Quadrit.B_, Quadrit.B_ => Quadrit.R_, _ => Quadrit.Y_ },
			Quadrit.Y_ => Quadrit.Y_,
			Quadrit.G_ => Quadrit.Y_,
			Quadrit.B_ => other.val switch { Quadrit.B_ => Quadrit.B_, Quadrit.R_ => Quadrit.R_, _ => Quadrit.Y_ }
		};
	}

	public Quadrit satisfies(in Quadrit other) {
		this.satisfies(in other, out Quadrit result);
		return result;
	}
	public void satisfies(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.val switch { Quadrit.R_ => Quadrit.B_, Quadrit.B_ => Quadrit.R_, _ => Quadrit.Y_ },
			Quadrit.Y_ => Quadrit.G_,
			Quadrit.G_ => Quadrit.G_,
			Quadrit.B_ => other.val switch { Quadrit.B_ => Quadrit.B_, Quadrit.R_ => Quadrit.R_, _ => Quadrit.Y_ }
		};
	}

	public Quadrit provides(in Quadrit other) {
		this.provides(in other, out Quadrit result);
		return result;
	}
	public void provides(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.val switch { Quadrit.B_ => Quadrit.R_, Quadrit.R_ => Quadrit.B_, _ => Quadrit.G_ },
			Quadrit.Y_ => Quadrit.Y_,
			Quadrit.G_ => Quadrit.Y_,
			Quadrit.B_ => other.val switch { Quadrit.R_ => Quadrit.R_, Quadrit.B_ => Quadrit.B_, _ => Quadrit.G_ }
		};
	}
	
	public Quadrit land(in Quadrit other) {
		this.land(in other, out Quadrit result);
		return result;
	}
	public void land(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.R_,
			Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.G_, Quadrit.G_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.B_ => other
		};
	}

	public Quadrit lor(in Quadrit other) {
		this.lor(in other, out Quadrit result);
		return result;
	}
	public void lor(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other,
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.Y_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.B_ => Quadrit.B_
		};
	}

	public Quadrit lnand(in Quadrit other) {
		this.lnand(in other, out Quadrit result);
		return result;
	}
	public void lnand(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.B_,
			Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.Y_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.B_ => other.not()
		};
	}

	public Quadrit lnor(in Quadrit other) {
		this.lnor(in other, out Quadrit result);
		return result;
	}
	public void lnor(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.not(),
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.B_ => Quadrit.R_
		};
	}

	public Quadrit lxor(in Quadrit other) {
		this.lxor(in other, out Quadrit result);
		return result;
	}
	public void lxor(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other,
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.Y_, Quadrit.Y_ => Quadrit.R_, Quadrit.G_ => Quadrit.Y_, Quadrit.B_ => Quadrit.G_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.Y_, Quadrit.G_ => Quadrit.R_, Quadrit.B_ => Quadrit.Y_ },
			Quadrit.B_ => other.not()
		};
	}

	public Quadrit leqv(in Quadrit other) {
		this.leqv(in other, out Quadrit result);
		return result;
	}
	public void leqv(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.not(),
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.B_, Quadrit.G_ => Quadrit.G_, Quadrit.B_ => Quadrit.Y_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.Y_, Quadrit.Y_ => Quadrit.G_, Quadrit.G_ => Quadrit.B_, Quadrit.B_ => Quadrit.G_ },
			Quadrit.B_ => other
		};
	}
	public Quadrit lncnv(in Quadrit other) {
		this.lncnv(in other, out Quadrit result);
		return result;
	}
	public void lncnv(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other,
			Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.G_, Quadrit.G_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.B_ => Quadrit.R_
		};
	}

	public Quadrit lnimp(in Quadrit other) {
		this.lnimp(in other, out Quadrit result);
		return result;
	}
	public void lnimp(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.R_,
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.G_, Quadrit.Y_ => Quadrit.Y_, _ => Quadrit.R_ },
			Quadrit.B_ => other.not()
		};
	}

	public Quadrit lcnv(in Quadrit other) {
		this.lcnv(in other, out Quadrit result);
		return result;
	}
	public void lcnv(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => other.not(),
			Quadrit.Y_ => other.val switch { Quadrit.B_ => Quadrit.Y_, Quadrit.G_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.G_ => other.val switch { Quadrit.B_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.B_ => Quadrit.B_
		};
	}

	public Quadrit limp(in Quadrit other) {
		this.limp(in other, out Quadrit result);
		return result;
	}
	public void limp(in Quadrit other, out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.B_,
			Quadrit.Y_ => other.val switch { Quadrit.R_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.G_ => other.val switch { Quadrit.R_ => Quadrit.Y_, Quadrit.Y_ => Quadrit.G_, _ => Quadrit.B_ },
			Quadrit.B_ => other
		};
	}

	public Quadrit affinity() {
		this.affinity(out Quadrit result);
		return result;
	}
	public void affinity(out Quadrit result) {
		result = this.val switch {
			Quadrit.R_ => Quadrit.R_,
			Quadrit.Y_ => Quadrit.R_,
			Quadrit.G_ => Quadrit.B_,
			Quadrit.B_ => Quadrit.B_
		};
	}
	
	public Trit tri() {
		this.tri(out Trit result);
		return result;
	}
	public void tri(out Trit result) {
		result = this.val switch {
			Quadrit.R_ => Trit.F,
			Quadrit.Y_ => Trit.U,
			Quadrit.G_ => Trit.U,
			Quadrit.B_ => Trit.T
		};
	}
	
	public Bit bi() {
		this.bi(out Bit result);
		return result;
	}
	public void bi(out Bit result) {
		result = this.val switch {
			Quadrit.R_ => Bit.F,
			Quadrit.Y_ => Bit.F,
			Quadrit.G_ => Bit.T,
			Quadrit.B_ => Bit.T
		};
	}
}

public readonly struct Trit:Signal<Trit>,BoolOps<Trit>,TernOps<Trit>,LucOps<Trit> {
    public Trit(byte val) {
	    this.val = val;
    }
    private const byte F_ = 0b01;
    private const byte U_ = 0b00;
    private const byte T_ = 0b10;
    public static readonly Trit F = F_;
    public static readonly Trit U = U_;
    public static readonly Trit T = T_;
    
    public static implicit operator byte(Trit input) => input.val;
    public static implicit operator Trit(byte input) => new(input);

    private readonly byte val;

    public static Trit Low => Trit.F_;
    public static Trit High => Trit.T_;

    public bool IsTruthy => this.val==0b10;
    public bool IsKnown => (this.val & 0b11) > 0b0;

    public override string ToString() {
	    return (byte)this switch {Trit.F_=>"FALSE",Trit.U_=>"UNKNOWN",Trit.T_=>"TRUE"};
    }
    
    public Trit and(in Trit other) {
		this.and(in other, out Trit result);
		return result;
	}
	public void and(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => other.val switch { Trit.F_ => Trit.F_, _ => Trit.U_ },
			Trit.T_ => other
		};
	}

	public Trit or(in Trit other) {
		this.or(in other, out Trit result);
		return result;
	}
	public void or(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => other.val switch { Trit.T_ => Trit.T_, _ => Trit.U_ },
			Trit.T_ => Trit.T_
		};
	}

	public Trit not() {
		this.not(out Trit result);
		return result;
	}
	public void not(out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => Trit.U_,
			Trit.T_ => Trit.F_
		};
	}

	public Trit nand(in Trit other) {
		this.nand(in other, out Trit result);
		return result;
	}
	public void nand(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => other.val switch { Trit.F_ => Trit.T_, _ => Trit.U_ },
			Trit.T_ => other.not()
		};
	}

	public Trit nor(in Trit other) {
		this.nor(in other, out Trit result);
		return result;
	}
	public void nor(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => other.val switch { Trit.T_ => Trit.F_, _ => Trit.U_ },
			Trit.T_ => Trit.F_
		};
	}

	public Trit xor(in Trit other) {
		this.xor(in other, out Trit result);
		return result;
	}
	public void xor(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => Trit.U_,
			Trit.T_ => other.not()
		};
	}

	public Trit eqv(in Trit other) {
		this.eqv(in other, out Trit result);
		return result;
	}
	public void eqv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => Trit.U_,
			Trit.T_ => other
		};
	}
	public Trit ncnv(in Trit other) {
		this.ncnv(in other, out Trit result);
		return result;
	}
	public void ncnv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => other.val switch { Trit.F_ => Trit.F_, _ => Trit.U_ },
			Trit.T_ => Trit.F_
		};
	}

	public Trit nimp(in Trit other) {
		this.nimp(in other, out Trit result);
		return result;
	}
	public void nimp(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => other.val switch { Trit.T_ => Trit.F_, _ => Trit.U_ },
			Trit.T_ => other.not()
		};
	}

	public Trit cnv(in Trit other) {
		this.cnv(in other, out Trit result);
		return result;
	}
	public void cnv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => other.val switch { Trit.F_ => Trit.T_, _ => Trit.U_ },
			Trit.T_ => Trit.T_
		};
	}

	public Trit imp(in Trit other) {
		this.imp(in other, out Trit result);
		return result;
	}
	public void imp(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => other.val switch { Trit.T_ => Trit.T_, _ => Trit.U_ },
			Trit.T_ => other
		};
	}
	
	
	public Trit known() {
		this.known(out Trit result);
		return result;
	}
	public void known(out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => Trit.F_,
			Trit.T_ => Trit.T_
		};
	}

	public Trit possible() {
		this.possible(out Trit result);
		return result;
	}
	public void possible(out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => Trit.T_,
			Trit.T_ => Trit.T_
		};
	}

	public Trit necessary() {
		this.necessary(out Trit result);
		return result;
	}
	public void necessary(out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => Trit.F_,
			Trit.T_ => Trit.T_
		};
	}

	public Trit over(in Trit other) {
		this.over(in other, out Trit result);
		return result;
	}
	public void over(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.val switch { Trit.T_ => Trit.F_, _ => Trit.T_ },
			Trit.U_ => Trit.T_,
			Trit.T_ => other.val switch { Trit.F_ => Trit.F_, _ => Trit.T_ }
		};
	}

	public Trit under(in Trit other) {
		this.under(in other, out Trit result);
		return result;
	}
	public void under(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.val switch { Trit.F_ => Trit.T_, _ => Trit.F_ },
			Trit.U_ => Trit.F_,
			Trit.T_ => other.val switch { Trit.T_ => Trit.T_, _ => Trit.F_ }
		};
	}

	public Trit satisfies(in Trit other) {
		this.satisfies(in other, out Trit result);
		return result;
	}
	public void satisfies(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.val switch { Trit.F_ => Trit.T_, _ => Trit.F_ },
			Trit.U_ => Trit.T_,
			Trit.T_ => other.val switch { Trit.T_ => Trit.T_, _ => Trit.F_ }
		};
	}

	public Trit provides(in Trit other) {
		this.provides(in other, out Trit result);
		return result;
	}
	public void provides(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.val switch { Trit.T_ => Trit.F_, _ => Trit.T_ },
			Trit.U_ => Trit.F_,
			Trit.T_ => other.val switch { Trit.F_ => Trit.F_, _ => Trit.T_ }
		};
	}
	
	public Trit land(in Trit other) {
		this.land(in other, out Trit result);
		return result;
	}
	public void land(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => other.val switch { Trit.T_ => Trit.U_, _ => Trit.F_ },
			Trit.T_ => other
		};
	}

	public Trit lor(in Trit other) {
		this.lor(in other, out Trit result);
		return result;
	}
	public void lor(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => other.val switch { Trit.F_ => Trit.U_, _ => Trit.T_ },
			Trit.T_ => Trit.T_
		};
	}

	public Trit lnand(in Trit other) {
		this.lnand(in other, out Trit result);
		return result;
	}
	public void lnand(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => other.val switch { Trit.T_ => Trit.U_, _ => Trit.T_ },
			Trit.T_ => other.not()
		};
	}

	public Trit lnor(in Trit other) {
		this.lnor(in other, out Trit result);
		return result;
	}
	public void lnor(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => other.val switch { Trit.F_ => Trit.U_, _ => Trit.F_ },
			Trit.T_ => Trit.F_
		};
	}

	public Trit lxor(in Trit other) {
		this.lxor(in other, out Trit result);
		return result;
	}
	public void lxor(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => other.val switch { Trit.U_ => Trit.F_, _ => Trit.U_ },
			Trit.T_ => other.not()
		};
	}

	public Trit leqv(in Trit other) {
		this.leqv(in other, out Trit result);
		return result;
	}
	public void leqv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => other.val switch { Trit.U_ => Trit.T_, _ => Trit.U_ },
			Trit.T_ => other
		};
	}
	public Trit lncnv(in Trit other) {
		this.lncnv(in other, out Trit result);
		return result;
	}
	public void lncnv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other,
			Trit.U_ => other.val switch { Trit.T_ => Trit.U_, _ => Trit.F_ },
			Trit.T_ => Trit.F_
		};
	}

	public Trit lnimp(in Trit other) {
		this.lnimp(in other, out Trit result);
		return result;
	}
	public void lnimp(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.F_,
			Trit.U_ => other.val switch { Trit.F_ => Trit.U_, _ => Trit.F_ },
			Trit.T_ => other.not()
		};
	}

	public Trit lcnv(in Trit other) {
		this.lcnv(in other, out Trit result);
		return result;
	}
	public void lcnv(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => other.not(),
			Trit.U_ => other.val switch { Trit.T_ => Trit.U_, _ => Trit.T_ },
			Trit.T_ => Trit.T_
		};
	}

	public Trit limp(in Trit other) {
		this.limp(in other, out Trit result);
		return result;
	}
	public void limp(in Trit other, out Trit result) {
		result = this.val switch {
			Trit.F_ => Trit.T_,
			Trit.U_ => other.val switch { Trit.F_ => Trit.U_, _ => Trit.T_ },
			Trit.T_ => other
		};
	}
}

public readonly struct Bit:Signal<Bit>,BoolOps<Bit> {
    public Bit(byte val) {
	    this.val = val;
    }
    
    private const byte F_ =0b0;
    private const byte T_ =0b1;
    public static readonly Bit F = F_;
    public static readonly Bit T = T_;
    
    public static implicit operator byte(Bit input) => input.val;
    public static implicit operator Bit(byte input) => new(input);

    private readonly byte val;

    public static Bit Low => Bit.F_;
    public static Bit High => Bit.T_;

    public bool IsTruthy => this.val==0b1;
    public bool IsKnown => true;

    public override string ToString() {
	    return (byte)this switch {Bit.F_=>"FALSE",Bit.T_=>"TRUE"};
    }
    
    public Bit not() {
		this.not(out Bit result);
		return result;
	}
	public void not(out Bit result) {
		result = this.val switch { Bit.F_ => Bit.T_, Bit.T_ => Bit.F_ };
	}

	public Bit and(in Bit other) {
		this.and(in other, out Bit result);
		return result;
	}
	public void and(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => Bit.F_, Bit.T_ => other };
	}

	public Bit nor(in Bit other) {
		this.nor(in other, out Bit result);
		return result;
	}
	public void nor(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other.not(), Bit.T_ => Bit.F_ };
	}

	public Bit ncnv(in Bit other) {
		this.ncnv(in other, out Bit result);
		return result;
	}
	public void ncnv(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other, Bit.T_ => Bit.F_ };
	}

	public Bit nimp(in Bit other) {
		this.nimp(in other, out Bit result);
		return result;
	}
	public void nimp(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => Bit.F_, Bit.T_ => other.not() };
	}

	public Bit or(in Bit other) {
		this.or(in other, out Bit result);
		return result;
	}
	public void or(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other, Bit.T_ => Bit.T_ };
	}

	public Bit nand(in Bit other) {
		this.nand(in other, out Bit result);
		return result;
	}
	public void nand(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => Bit.T_, Bit.T_ => other.not() };
	}

	public Bit cnv(in Bit other) {
		this.cnv(in other, out Bit result);
		return result;
	}
	public void cnv(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other.not(), Bit.T_ => Bit.T_ };
	}

	public Bit imp(in Bit other) {
		this.imp(in other, out Bit result);
		return result;
	}
	public void imp(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => Bit.T_, Bit.T_ => other };
	}

	public Bit xor(in Bit other) {
		this.xor(in other, out Bit result);
		return result;
	}
	public void xor(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other, Bit.T_ => other.not() };
	}

	public Bit eqv(in Bit other) {
		this.eqv(in other, out Bit result);
		return result;
	}
	public void eqv(in Bit other, out Bit result) {
		result = this.val switch { Bit.F_ => other.not(), Bit.T_ => other };
	}
	
}