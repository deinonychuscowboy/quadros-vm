using QuadrosLIS.Util;

namespace QuadrosLIS.Model;
#pragma warning disable CS8509
// ReSharper disable ArrangeStaticMemberQualifier

// basic gate structural stuff

/// Takes a given input and does something to it
public interface Operator<T> where T:Signal<T> {
	void input(int index, T value);
}

/// Accepts a given output and transmits it unchanged to some location
public interface Transmitter<T> where T:Signal<T> {
	void output(T value);
	T read();
}

public delegate T[] Calc<T>(T[] inputs);

/// Object which takes inputs, performs some calculation with them, and passes the resulting outputs to a transmitter
/// Abstract implementation with no actual operation, so not public
public abstract class Gate<T>:Operator<T> where T:Signal<T> {
	private readonly T[] inputs;
	private readonly T[] values;
	private readonly Transmitter<T>[] outputs;
	private readonly Calc<T> calc;

	protected Gate(T[] inputs, T[] values, Transmitter<T>[] outputs, Calc<T> calc) {
		this.inputs = inputs;
		this.values = values;
		this.outputs = outputs;
		this.calc = calc;
	}

	private void output() {
		foreach(int key in 0..this.outputs.Length) {
			this.outputs[key].output(this.values[key]);
		}
	}
	private void mutate(int index, T value) {
		this.inputs[index]=value;
		T[] new_values=this.calc(this.inputs);
		foreach(int key in 0..new_values.Length){
			this.values[key]=new_values[key];
		}
	}
	
	public void input(int index, T value) {
		this.mutate(index, value);
		this.output();
	}
}

/// Object which takes inputs and transmits them to a destination operator
public class Link<T>:Transmitter<T> where T:Signal<T> {
	private readonly Operator<T>? destination;
	private readonly int index;
	private T value;
	
	/// Terminator
	public static Link<T> new_terminal(){
		Link<T> link=new(
			null,
			0
		);

		return link;
	}
	/// Link to destination
	public Link(Operator<T>? destination, int index) {
		this.destination = destination;
		this.index = index;
		this.value = T.Low;
	}

	private void input() {
		if(this.destination is not null){
			this.destination.input(this.index, this.value);
		}
	}
	private void mutate(T value) {
		this.value=value;
	}
	
	public void output(T value) {
		this.mutate(value);
		this.input();
	}
	
	public T read() {
		return this.value;
	}

	public override string ToString() {
		return this.value.ToString()!;
	}
}

// specific implementations of gates that call particular logic operators on their inputs

// Special gates

/// Tie is a special gate that takes a single input and outputs whatever is given on that input to any number of outputs.
public class Tie<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public Tie(Transmitter<T>[] outputs):base(
		new[]{T.Low},
		Enumerable.Repeat(T.Low,outputs.Length).ToArray(),
		outputs,
		t=>Enumerable.Repeat(t[0],outputs.Length).ToArray()
	){}
}

// unary boolean operations (one input, one output)
public class NotGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public NotGate(Transmitter<T>[] outputs):base(
		new[]{T.Low},
		new[]{T.Low},
		outputs,
		t=>new[]{t[0].not()}
	){}
}

// variable-width aggregating boolean operations (n inputs, one output)
public class AndGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public AndGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.and(e))}
	){}
}
public class NorGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public NorGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.nor(e))}
	){}
}
public class NimpGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public NimpGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.nimp(e))}
	){}
}
public class NcnvGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public NcnvGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.ncnv(e))}
	){}
}
public class OrGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public OrGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.or(e))}
	){}
}
public class NandGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public NandGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.nand(e))}
	){}
}
public class ImpGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public ImpGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.imp(e))}
	){}
}
public class CnvGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public CnvGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.cnv(e))}
	){}
}
public class XorGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public XorGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.xor(e))}
	){}
}
public class EqvGate<T> : Gate<T> where T : Signal<T>, BoolOps<T> {
	public EqvGate(int input_count,Transmitter<T>[] outputs):base(
		Enumerable.Repeat(T.Low,input_count).ToArray(),
		new[]{T.Low},
		outputs,
		t=>new[]{t.Skip(1).Aggregate(t[0],(c, e) => c.eqv(e))}
	){}
}