using static System.Diagnostics.Trace;
using static QuadrosLIS.Model.Bit;
// ReSharper disable ArrangeStaticMemberQualifier

namespace QuadrosLIS.Test;

public static class BitTest {
    
    public static void test(){
        BitTest.test_and();
        BitTest.test_or();
        BitTest.test_not();
        BitTest.test_nand();
        BitTest.test_nor();
        BitTest.test_xor();
        BitTest.test_eqv();
        BitTest.test_imp();
        BitTest.test_cnv();
        BitTest.test_nimp();
        BitTest.test_ncnv();
    }

    private static void test_and(){
        Assert(F==F.and(F));
        Assert(F==F.and(T));

        Assert(F==T.and(F));
        Assert(T==T.and(T));
    }

    private static void test_or(){
        Assert(F==F.or(F));
        Assert(T==F.or(T));

        Assert(T==T.or(F));
        Assert(T==T.or(T));
    }

    private static void test_not(){
        Assert(F==T.not());
        Assert(T==F.not());
    }

    private static void test_nand(){
        Assert(T==F.nand(F));
        Assert(T==F.nand(T));

        Assert(T==T.nand(F));
        Assert(F==T.nand(T));
    }

    private static void test_nor(){
        Assert(T==F.nor(F));
        Assert(F==F.nor(T));

        Assert(F==T.nor(F));
        Assert(F==T.nor(T));
    }

    private static void test_xor(){
        Assert(F==F.xor(F));
        Assert(T==F.xor(T));

        Assert(T==T.xor(F));
        Assert(F==T.xor(T));
    }

    private static void test_eqv(){
        Assert(T==F.eqv(F));
        Assert(F==F.eqv(T));

        Assert(F==T.eqv(F));
        Assert(T==T.eqv(T));
    }

    private static void test_cnv(){
        Assert(T==F.cnv(F));
        Assert(F==F.cnv(T));

        Assert(T==T.cnv(F));
        Assert(T==T.cnv(T));
    }

    private static void test_imp(){
        Assert(T==F.imp(F));
        Assert(T==F.imp(T));

        Assert(F==T.imp(F));
        Assert(T==T.imp(T));
    }

    private static void test_ncnv(){
        Assert(F==F.ncnv(F));
        Assert(T==F.ncnv(T));

        Assert(F==T.ncnv(F));
        Assert(F==T.ncnv(T));
    }

    private static void test_nimp(){
        Assert(F==F.nimp(F));
        Assert(F==F.nimp(T));

        Assert(T==T.nimp(F));
        Assert(F==T.nimp(T));
    }
} 