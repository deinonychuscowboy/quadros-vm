using static System.Diagnostics.Trace;
using static QuadrosLIS.Model.Quadrit;
// ReSharper disable ArrangeStaticMemberQualifier

namespace QuadrosLIS.Test;

public static class QuadritTest {
	public static void test() {
		QuadritTest.test_and();
		QuadritTest.test_or();
		QuadritTest.test_not();
		QuadritTest.test_nand();
		QuadritTest.test_nor();
		QuadritTest.test_xor();
		QuadritTest.test_eqv();
		QuadritTest.test_imp();
		QuadritTest.test_cnv();
		QuadritTest.test_nimp();
		QuadritTest.test_ncnv();
		QuadritTest.test_land();
		QuadritTest.test_lor();
		QuadritTest.test_lnand();
		QuadritTest.test_lnor();
		QuadritTest.test_lxor();
		QuadritTest.test_leqv();
		QuadritTest.test_limp();
		QuadritTest.test_lcnv();
		QuadritTest.test_lnimp();
		QuadritTest.test_lncnv();
		QuadritTest.test_known();
		QuadritTest.test_possible();
		QuadritTest.test_necessary();
		QuadritTest.test_over();
		QuadritTest.test_under();
		QuadritTest.test_satisfies();
		QuadritTest.test_provides();
		QuadritTest.test_affinity();
	}

	private static void test_and() {
		Assert(R==R.and(R));
		Assert(R==R.and(Y));
		Assert(R==R.and(G));
		Assert(R==R.and(B));

		Assert(R==Y.and(R));
		Assert(Y==Y.and(Y));
		Assert(Y==Y.and(G));
		Assert(Y==Y.and(B));

		Assert(R==G.and(R));
		Assert(Y==G.and(Y));
		Assert(G==G.and(G));
		Assert(G==G.and(B));

		Assert(R==B.and(R));
		Assert(Y==B.and(Y));
		Assert(G==B.and(G));
		Assert(B==B.and(B));
	}

	private static void test_or() {
		Assert(R==R.or(R));
		Assert(Y==R.or(Y));
		Assert(G==R.or(G));
		Assert(B==R.or(B));

		Assert(Y==Y.or(R));
		Assert(Y==Y.or(Y));
		Assert(G==Y.or(G));
		Assert(B==Y.or(B));

		Assert(G==G.or(R));
		Assert(G==G.or(Y));
		Assert(G==G.or(G));
		Assert(B==G.or(B));

		Assert(B==B.or(R));
		Assert(B==B.or(Y));
		Assert(B==B.or(G));
		Assert(B==B.or(B));
	}

	private static void test_not() {
		Assert(R==B.not());
		Assert(Y==G.not());
		Assert(G==Y.not());
		Assert(B==R.not());
	}

	private static void test_nand() {
		Assert(B==R.nand(R));
		Assert(B==R.nand(Y));
		Assert(B==R.nand(G));
		Assert(B==R.nand(B));

		Assert(B==Y.nand(R));
		Assert(G==Y.nand(Y));
		Assert(G==Y.nand(G));
		Assert(G==Y.nand(B));

		Assert(B==G.nand(R));
		Assert(G==G.nand(Y));
		Assert(Y==G.nand(G));
		Assert(Y==G.nand(B));

		Assert(B==B.nand(R));
		Assert(G==B.nand(Y));
		Assert(Y==B.nand(G));
		Assert(R==B.nand(B));
	}

	private static void test_nor() {
		Assert(B==R.nor(R));
		Assert(G==R.nor(Y));
		Assert(Y==R.nor(G));
		Assert(R==R.nor(B));

		Assert(G==Y.nor(R));
		Assert(G==Y.nor(Y));
		Assert(Y==Y.nor(G));
		Assert(R==Y.nor(B));

		Assert(Y==G.nor(R));
		Assert(Y==G.nor(Y));
		Assert(Y==G.nor(G));
		Assert(R==G.nor(B));

		Assert(R==B.nor(R));
		Assert(R==B.nor(Y));
		Assert(R==B.nor(G));
		Assert(R==B.nor(B));
	}

	private static void test_xor() {
		Assert(R==R.xor(R));
		Assert(Y==R.xor(Y));
		Assert(G==R.xor(G));
		Assert(B==R.xor(B));

		Assert(Y==Y.xor(R));
		Assert(Y==Y.xor(Y));
		Assert(G==Y.xor(G));
		Assert(G==Y.xor(B));

		Assert(G==G.xor(R));
		Assert(G==G.xor(Y));
		Assert(Y==G.xor(G));
		Assert(Y==G.xor(B));

		Assert(B==B.xor(R));
		Assert(G==B.xor(Y));
		Assert(Y==B.xor(G));
		Assert(R==B.xor(B));
	}

	private static void test_eqv() {
		Assert(B==R.eqv(R));
		Assert(G==R.eqv(Y));
		Assert(Y==R.eqv(G));
		Assert(R==R.eqv(B));

		Assert(G==Y.eqv(R));
		Assert(G==Y.eqv(Y));
		Assert(Y==Y.eqv(G));
		Assert(Y==Y.eqv(B));

		Assert(Y==G.eqv(R));
		Assert(Y==G.eqv(Y));
		Assert(G==G.eqv(G));
		Assert(G==G.eqv(B));

		Assert(R==B.eqv(R));
		Assert(Y==B.eqv(Y));
		Assert(G==B.eqv(G));
		Assert(B==B.eqv(B));
	}

	private static void test_cnv() {
		Assert(B==R.cnv(R));
		Assert(G==R.cnv(Y));
		Assert(Y==R.cnv(G));
		Assert(R==R.cnv(B));

		Assert(B==Y.cnv(R));
		Assert(G==Y.cnv(Y));
		Assert(Y==Y.cnv(G));
		Assert(Y==Y.cnv(B));

		Assert(B==G.cnv(R));
		Assert(G==G.cnv(Y));
		Assert(G==G.cnv(G));
		Assert(G==G.cnv(B));

		Assert(B==B.cnv(R));
		Assert(B==B.cnv(Y));
		Assert(B==B.cnv(G));
		Assert(B==B.cnv(B));
	}

	private static void test_imp() {
		Assert(B==R.imp(R));
		Assert(B==R.imp(Y));
		Assert(B==R.imp(G));
		Assert(B==R.imp(B));

		Assert(G==Y.imp(R));
		Assert(G==Y.imp(Y));
		Assert(G==Y.imp(G));
		Assert(B==Y.imp(B));

		Assert(Y==G.imp(R));
		Assert(Y==G.imp(Y));
		Assert(G==G.imp(G));
		Assert(B==G.imp(B));

		Assert(R==B.imp(R));
		Assert(Y==B.imp(Y));
		Assert(G==B.imp(G));
		Assert(B==B.imp(B));
	}

	private static void test_ncnv() {
		Assert(R==R.ncnv(R));
		Assert(Y==R.ncnv(Y));
		Assert(G==R.ncnv(G));
		Assert(B==R.ncnv(B));

		Assert(R==Y.ncnv(R));
		Assert(Y==Y.ncnv(Y));
		Assert(G==Y.ncnv(G));
		Assert(G==Y.ncnv(B));

		Assert(R==G.ncnv(R));
		Assert(Y==G.ncnv(Y));
		Assert(Y==G.ncnv(G));
		Assert(Y==G.ncnv(B));

		Assert(R==B.ncnv(R));
		Assert(R==B.ncnv(Y));
		Assert(R==B.ncnv(G));
		Assert(R==B.ncnv(B));
	}

	private static void test_nimp() {
		Assert(R==R.nimp(R));
		Assert(R==R.nimp(Y));
		Assert(R==R.nimp(G));
		Assert(R==R.nimp(B));

		Assert(Y==Y.nimp(R));
		Assert(Y==Y.nimp(Y));
		Assert(Y==Y.nimp(G));
		Assert(R==Y.nimp(B));

		Assert(G==G.nimp(R));
		Assert(G==G.nimp(Y));
		Assert(Y==G.nimp(G));
		Assert(R==G.nimp(B));

		Assert(B==B.nimp(R));
		Assert(G==B.nimp(Y));
		Assert(Y==B.nimp(G));
		Assert(R==B.nimp(B));
	}

	private static void test_land() {
		Assert(R==R.land(R));
		Assert(R==R.land(Y));
		Assert(R==R.land(G));
		Assert(R==R.land(B));

		Assert(R==Y.land(R));
		Assert(R==Y.land(Y));
		Assert(R==Y.land(G));
		Assert(Y==Y.land(B));

		Assert(R==G.land(R));
		Assert(R==G.land(Y));
		Assert(Y==G.land(G));
		Assert(G==G.land(B));

		Assert(R==B.land(R));
		Assert(Y==B.land(Y));
		Assert(G==B.land(G));
		Assert(B==B.land(B));
	}

	private static void test_lor() {
		Assert(R==R.lor(R));
		Assert(Y==R.lor(Y));
		Assert(G==R.lor(G));
		Assert(B==R.lor(B));

		Assert(Y==Y.lor(R));
		Assert(G==Y.lor(Y));
		Assert(B==Y.lor(G));
		Assert(B==Y.lor(B));

		Assert(G==G.lor(R));
		Assert(B==G.lor(Y));
		Assert(B==G.lor(G));
		Assert(B==G.lor(B));

		Assert(B==B.lor(R));
		Assert(B==B.lor(Y));
		Assert(B==B.lor(G));
		Assert(B==B.lor(B));
	}

	private static void test_lnand() {
		Assert(B==R.lnand(R));
		Assert(B==R.lnand(Y));
		Assert(B==R.lnand(G));
		Assert(B==R.lnand(B));

		Assert(B==Y.lnand(R));
		Assert(B==Y.lnand(Y));
		Assert(B==Y.lnand(G));
		Assert(G==Y.lnand(B));

		Assert(B==G.lnand(R));
		Assert(B==G.lnand(Y));
		Assert(G==G.lnand(G));
		Assert(Y==G.lnand(B));

		Assert(B==B.lnand(R));
		Assert(G==B.lnand(Y));
		Assert(Y==B.lnand(G));
		Assert(R==B.lnand(B));
	}

	private static void test_lnor() {
		Assert(B==R.lnor(R));
		Assert(G==R.lnor(Y));
		Assert(Y==R.lnor(G));
		Assert(R==R.lnor(B));

		Assert(G==Y.lnor(R));
		Assert(Y==Y.lnor(Y));
		Assert(R==Y.lnor(G));
		Assert(R==Y.lnor(B));

		Assert(Y==G.lnor(R));
		Assert(R==G.lnor(Y));
		Assert(R==G.lnor(G));
		Assert(R==G.lnor(B));

		Assert(R==B.lnor(R));
		Assert(R==B.lnor(Y));
		Assert(R==B.lnor(G));
		Assert(R==B.lnor(B));
	}

	private static void test_lxor() {
		Assert(R==R.lxor(R));
		Assert(Y==R.lxor(Y));
		Assert(G==R.lxor(G));
		Assert(B==R.lxor(B));

		Assert(Y==Y.lxor(R));
		Assert(R==Y.lxor(Y));
		Assert(Y==Y.lxor(G));
		Assert(G==Y.lxor(B));

		Assert(G==G.lxor(R));
		Assert(Y==G.lxor(Y));
		Assert(R==G.lxor(G));
		Assert(Y==G.lxor(B));

		Assert(B==B.lxor(R));
		Assert(G==B.lxor(Y));
		Assert(Y==B.lxor(G));
		Assert(R==B.lxor(B));
	}

	private static void test_leqv() {
		Assert(B==R.leqv(R));
		Assert(G==R.leqv(Y));
		Assert(Y==R.leqv(G));
		Assert(R==R.leqv(B));

		Assert(G==Y.leqv(R));
		Assert(B==Y.leqv(Y));
		Assert(G==Y.leqv(G));
		Assert(Y==Y.leqv(B));

		Assert(Y==G.leqv(R));
		Assert(G==G.leqv(Y));
		Assert(B==G.leqv(G));
		Assert(G==G.leqv(B));

		Assert(R==B.leqv(R));
		Assert(Y==B.leqv(Y));
		Assert(G==B.leqv(G));
		Assert(B==B.leqv(B));
	}

	private static void test_lcnv() {
		Assert(B==R.lcnv(R));
		Assert(G==R.lcnv(Y));
		Assert(Y==R.lcnv(G));
		Assert(R==R.lcnv(B));

		Assert(B==Y.lcnv(R));
		Assert(B==Y.lcnv(Y));
		Assert(G==Y.lcnv(G));
		Assert(Y==Y.lcnv(B));

		Assert(B==G.lcnv(R));
		Assert(B==G.lcnv(Y));
		Assert(B==G.lcnv(G));
		Assert(G==G.lcnv(B));

		Assert(B==B.lcnv(R));
		Assert(B==B.lcnv(Y));
		Assert(B==B.lcnv(G));
		Assert(B==B.lcnv(B));
	}

	private static void test_limp() {
		Assert(B==R.limp(R));
		Assert(B==R.limp(Y));
		Assert(B==R.limp(G));
		Assert(B==R.limp(B));

		Assert(G==Y.limp(R));
		Assert(B==Y.limp(Y));
		Assert(B==Y.limp(G));
		Assert(B==Y.limp(B));

		Assert(Y==G.limp(R));
		Assert(G==G.limp(Y));
		Assert(B==G.limp(G));
		Assert(B==G.limp(B));

		Assert(R==B.limp(R));
		Assert(Y==B.limp(Y));
		Assert(G==B.limp(G));
		Assert(B==B.limp(B));
	}

	private static void test_lncnv() {
		Assert(R==R.lncnv(R));
		Assert(Y==R.lncnv(Y));
		Assert(G==R.lncnv(G));
		Assert(B==R.lncnv(B));

		Assert(R==Y.lncnv(R));
		Assert(R==Y.lncnv(Y));
		Assert(Y==Y.lncnv(G));
		Assert(G==Y.lncnv(B));

		Assert(R==G.lncnv(R));
		Assert(R==G.lncnv(Y));
		Assert(R==G.lncnv(G));
		Assert(Y==G.lncnv(B));

		Assert(R==B.lncnv(R));
		Assert(R==B.lncnv(Y));
		Assert(R==B.lncnv(G));
		Assert(R==B.lncnv(B));
	}

	private static void test_lnimp() {
		Assert(R==R.lnimp(R));
		Assert(R==R.lnimp(Y));
		Assert(R==R.lnimp(G));
		Assert(R==R.lnimp(B));

		Assert(Y==Y.lnimp(R));
		Assert(R==Y.lnimp(Y));
		Assert(R==Y.lnimp(G));
		Assert(R==Y.lnimp(B));

		Assert(G==G.lnimp(R));
		Assert(Y==G.lnimp(Y));
		Assert(R==G.lnimp(G));
		Assert(R==G.lnimp(B));

		Assert(B==B.lnimp(R));
		Assert(G==B.lnimp(Y));
		Assert(Y==B.lnimp(G));
		Assert(R==B.lnimp(B));
	}

	private static void test_known() {
		Assert(B==R.known());
		Assert(R==Y.known());
		Assert(R==G.known());
		Assert(B==B.known());
	}

	private static void test_possible() {
		Assert(R==R.possible());
		Assert(B==Y.possible());
		Assert(B==G.possible());
		Assert(B==B.possible());
	}

	private static void test_necessary() {
		Assert(R==R.necessary());
		Assert(R==Y.necessary());
		Assert(R==G.necessary());
		Assert(B==B.necessary());
	}

	private static void test_over() {
		Assert(B==R.over(R));
		Assert(G==R.over(Y));
		Assert(G==R.over(G));
		Assert(R==R.over(B));

		Assert(G==Y.over(R));
		Assert(G==Y.over(Y));
		Assert(G==Y.over(G));
		Assert(G==Y.over(B));

		Assert(G==G.over(R));
		Assert(G==G.over(Y));
		Assert(G==G.over(G));
		Assert(G==G.over(B));

		Assert(R==B.over(R));
		Assert(G==B.over(Y));
		Assert(G==B.over(G));
		Assert(B==B.over(B));
	}

	private static void test_under() {
		Assert(B==R.under(R));
		Assert(Y==R.under(Y));
		Assert(Y==R.under(G));
		Assert(R==R.under(B));

		Assert(Y==Y.under(R));
		Assert(Y==Y.under(Y));
		Assert(Y==Y.under(G));
		Assert(Y==Y.under(B));

		Assert(Y==G.under(R));
		Assert(Y==G.under(Y));
		Assert(Y==G.under(G));
		Assert(Y==G.under(B));

		Assert(R==B.under(R));
		Assert(Y==B.under(Y));
		Assert(Y==B.under(G));
		Assert(B==B.under(B));
	}

	private static void test_satisfies() {
		Assert(B==R.satisfies(R));
		Assert(Y==R.satisfies(Y));
		Assert(Y==R.satisfies(G));
		Assert(R==R.satisfies(B));

		Assert(G==Y.satisfies(R));
		Assert(G==Y.satisfies(Y));
		Assert(G==Y.satisfies(G));
		Assert(G==Y.satisfies(B));

		Assert(G==G.satisfies(R));
		Assert(G==G.satisfies(Y));
		Assert(G==G.satisfies(G));
		Assert(G==G.satisfies(B));

		Assert(R==B.satisfies(R));
		Assert(Y==B.satisfies(Y));
		Assert(Y==B.satisfies(G));
		Assert(B==B.satisfies(B));
	}


	private static void test_provides() {
		Assert(B==R.provides(R));
		Assert(G==R.provides(Y));
		Assert(G==R.provides(G));
		Assert(R==R.provides(B));

		Assert(Y==Y.provides(R));
		Assert(Y==Y.provides(Y));
		Assert(Y==Y.provides(G));
		Assert(Y==Y.provides(B));

		Assert(Y==G.provides(R));
		Assert(Y==G.provides(Y));
		Assert(Y==G.provides(G));
		Assert(Y==G.provides(B));

		Assert(R==B.provides(R));
		Assert(G==B.provides(Y));
		Assert(G==B.provides(G));
		Assert(B==B.provides(B));
	}

	private static void test_affinity() {
		Assert(R==R.affinity());
		Assert(R==Y.affinity());
		Assert(B==G.affinity());
		Assert(B==B.affinity());
	}
}