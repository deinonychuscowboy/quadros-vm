using static System.Diagnostics.Trace;
using static QuadrosLIS.Model.Trit;
// ReSharper disable ArrangeStaticMemberQualifier

namespace QuadrosLIS.Test;

public static class TritTest {

	public static void test() {
		TritTest.test_and();
		TritTest.test_or();
		TritTest.test_not();
		TritTest.test_nand();
		TritTest.test_nor();
		TritTest.test_xor();
		TritTest.test_eqv();
		TritTest.test_imp();
		TritTest.test_cnv();
		TritTest.test_nimp();
		TritTest.test_ncnv();
		TritTest.test_land();
		TritTest.test_lor();
		TritTest.test_lnand();
		TritTest.test_lnor();
		TritTest.test_lxor();
		TritTest.test_leqv();
		TritTest.test_limp();
		TritTest.test_lcnv();
		TritTest.test_lnimp();
		TritTest.test_lncnv();
		TritTest.test_known();
		TritTest.test_possible();
		TritTest.test_necessary();
		TritTest.test_over();
		TritTest.test_under();
		TritTest.test_satisfies();
		TritTest.test_provides();
	}

	private static void test_and() {
		Assert(F==F.and(F));
		Assert(F==F.and(U));
		Assert(F==F.and(T));

		Assert(F==U.and(F));
		Assert(U==U.and(U));
		Assert(U==U.and(T));

		Assert(F==T.and(F));
		Assert(U==T.and(U));
		Assert(T==T.and(T));
	}

	private static void test_or() {
		Assert(F==F.or(F));
		Assert(U==F.or(U));
		Assert(T==F.or(T));

		Assert(U==U.or(F));
		Assert(U==U.or(U));
		Assert(T==U.or(T));

		Assert(T==T.or(F));
		Assert(T==T.or(U));
		Assert(T==T.or(T));
	}

	private static void test_not() {
		Assert(F==T.not());
		Assert(U==U.not());
		Assert(T==F.not());
	}

	private static void test_nand() {
		Assert(T==F.nand(F));
		Assert(T==F.nand(U));
		Assert(T==F.nand(T));

		Assert(T==U.nand(F));
		Assert(U==U.nand(U));
		Assert(U==U.nand(T));

		Assert(T==T.nand(F));
		Assert(U==T.nand(U));
		Assert(F==T.nand(T));
	}

	private static void test_nor() {
		Assert(T==F.nor(F));
		Assert(U==F.nor(U));
		Assert(F==F.nor(T));

		Assert(U==U.nor(F));
		Assert(U==U.nor(U));
		Assert(F==U.nor(T));

		Assert(F==T.nor(F));
		Assert(F==T.nor(U));
		Assert(F==T.nor(T));
	}

	private static void test_xor() {
		Assert(F==F.xor(F));
		Assert(U==F.xor(U));
		Assert(T==F.xor(T));

		Assert(U==U.xor(F));
		Assert(U==U.xor(U));
		Assert(U==U.xor(T));

		Assert(T==T.xor(F));
		Assert(U==T.xor(U));
		Assert(F==T.xor(T));
	}

	private static void test_eqv() {
		Assert(T==F.eqv(F));
		Assert(U==F.eqv(U));
		Assert(F==F.eqv(T));

		Assert(U==U.eqv(F));
		Assert(U==U.eqv(U));
		Assert(U==U.eqv(T));

		Assert(F==T.eqv(F));
		Assert(U==T.eqv(U));
		Assert(T==T.eqv(T));
	}

	private static void test_cnv() {
		Assert(T==F.cnv(F));
		Assert(U==F.cnv(U));
		Assert(F==F.cnv(T));

		Assert(T==U.cnv(F));
		Assert(U==U.cnv(U));
		Assert(U==U.cnv(T));

		Assert(T==T.cnv(F));
		Assert(T==T.cnv(U));
		Assert(T==T.cnv(T));
	}

	private static void test_imp() {
		Assert(T==F.imp(F));
		Assert(T==F.imp(U));
		Assert(T==F.imp(T));

		Assert(U==U.imp(F));
		Assert(U==U.imp(U));
		Assert(T==U.imp(T));

		Assert(F==T.imp(F));
		Assert(U==T.imp(U));
		Assert(T==T.imp(T));
	}

	private static void test_ncnv() {
		Assert(F==F.ncnv(F));
		Assert(U==F.ncnv(U));
		Assert(T==F.ncnv(T));

		Assert(F==U.ncnv(F));
		Assert(U==U.ncnv(U));
		Assert(U==U.ncnv(T));

		Assert(F==T.ncnv(F));
		Assert(F==T.ncnv(U));
		Assert(F==T.ncnv(T));
	}

	private static void test_nimp() {
		Assert(F==F.nimp(F));
		Assert(F==F.nimp(U));
		Assert(F==F.nimp(T));

		Assert(U==U.nimp(F));
		Assert(U==U.nimp(U));
		Assert(F==U.nimp(T));

		Assert(T==T.nimp(F));
		Assert(U==T.nimp(U));
		Assert(F==T.nimp(T));
	}



	private static void test_land() {
		Assert(F==F.land(F));
		Assert(F==F.land(U));
		Assert(F==F.land(T));

		Assert(F==U.land(F));
		Assert(F==U.land(U));
		Assert(U==U.land(T));

		Assert(F==T.land(F));
		Assert(U==T.land(U));
		Assert(T==T.land(T));
	}

	private static void test_lor() {
		Assert(F==F.lor(F));
		Assert(U==F.lor(U));
		Assert(T==F.lor(T));

		Assert(U==U.lor(F));
		Assert(T==U.lor(U));
		Assert(T==U.lor(T));

		Assert(T==T.lor(F));
		Assert(T==T.lor(U));
		Assert(T==T.lor(T));
	}

	private static void test_lnand() {
		Assert(T==F.lnand(F));
		Assert(T==F.lnand(U));
		Assert(T==F.lnand(T));

		Assert(T==U.lnand(F));
		Assert(T==U.lnand(U));
		Assert(U==U.lnand(T));

		Assert(T==T.lnand(F));
		Assert(U==T.lnand(U));
		Assert(F==T.lnand(T));
	}

	private static void test_lnor() {
		Assert(T==F.lnor(F));
		Assert(U==F.lnor(U));
		Assert(F==F.lnor(T));

		Assert(U==U.lnor(F));
		Assert(F==U.lnor(U));
		Assert(F==U.lnor(T));

		Assert(F==T.lnor(F));
		Assert(F==T.lnor(U));
		Assert(F==T.lnor(T));
	}

	private static void test_lxor() {
		Assert(F==F.lxor(F));
		Assert(U==F.lxor(U));
		Assert(T==F.lxor(T));

		Assert(U==U.lxor(F));
		Assert(F==U.lxor(U));
		Assert(U==U.lxor(T));

		Assert(T==T.lxor(F));
		Assert(U==T.lxor(U));
		Assert(F==T.lxor(T));
	}

	private static void test_leqv() {
		Assert(T==F.leqv(F));
		Assert(U==F.leqv(U));
		Assert(F==F.leqv(T));

		Assert(U==U.leqv(F));
		Assert(T==U.leqv(U));
		Assert(U==U.leqv(T));

		Assert(F==T.leqv(F));
		Assert(U==T.leqv(U));
		Assert(T==T.leqv(T));
	}

	private static void test_lcnv() {
		Assert(T==F.lcnv(F));
		Assert(U==F.lcnv(U));
		Assert(F==F.lcnv(T));

		Assert(T==U.lcnv(F));
		Assert(T==U.lcnv(U));
		Assert(U==U.lcnv(T));

		Assert(T==T.lcnv(F));
		Assert(T==T.lcnv(U));
		Assert(T==T.lcnv(T));
	}

	private static void test_limp() {
		Assert(T==F.limp(F));
		Assert(T==F.limp(U));
		Assert(T==F.limp(T));

		Assert(U==U.limp(F));
		Assert(T==U.limp(U));
		Assert(T==U.limp(T));

		Assert(F==T.limp(F));
		Assert(U==T.limp(U));
		Assert(T==T.limp(T));
	}

	private static void test_lncnv() {
		Assert(F==F.lncnv(F));
		Assert(U==F.lncnv(U));
		Assert(T==F.lncnv(T));

		Assert(F==U.lncnv(F));
		Assert(F==U.lncnv(U));
		Assert(U==U.lncnv(T));

		Assert(F==T.lncnv(F));
		Assert(F==T.lncnv(U));
		Assert(F==T.lncnv(T));
	}

	private static void test_lnimp() {
		Assert(F==F.lnimp(F));
		Assert(F==F.lnimp(U));
		Assert(F==F.lnimp(T));

		Assert(U==U.lnimp(F));
		Assert(F==U.lnimp(U));
		Assert(F==U.lnimp(T));

		Assert(T==T.lnimp(F));
		Assert(U==T.lnimp(U));
		Assert(F==T.lnimp(T));
	}

	private static void test_known() {
		Assert(T==F.known());
		Assert(F==U.known());
		Assert(T==T.known());
	}

	private static void test_possible() {
		Assert(F==F.possible());
		Assert(T==U.possible());
		Assert(T==T.possible());
	}

	private static void test_necessary() {
		Assert(F==F.necessary());
		Assert(F==U.necessary());
		Assert(T==T.necessary());
	}

	private static void test_over() {
		Assert(T==F.over(F));
		Assert(T==F.over(U));
		Assert(F==F.over(T));

		Assert(T==U.over(F));
		Assert(T==U.over(U));
		Assert(T==U.over(T));

		Assert(F==T.over(F));
		Assert(T==T.over(U));
		Assert(T==T.over(T));
	}

	private static void test_under() {
		Assert(T==F.under(F));
		Assert(F==F.under(U));
		Assert(F==F.under(T));

		Assert(F==U.under(F));
		Assert(F==U.under(U));
		Assert(F==U.under(T));

		Assert(F==T.under(F));
		Assert(F==T.under(U));
		Assert(T==T.under(T));
	}

	private static void test_satisfies() {
		Assert(T==F.satisfies(F));
		Assert(F==F.satisfies(U));
		Assert(F==F.satisfies(T));

		Assert(T==U.satisfies(F));
		Assert(T==U.satisfies(U));
		Assert(T==U.satisfies(T));

		Assert(F==T.satisfies(F));
		Assert(F==T.satisfies(U));
		Assert(T==T.satisfies(T));
	}


	private static void test_provides() {
		Assert(T==F.provides(F));
		Assert(T==F.provides(U));
		Assert(F==F.provides(T));

		Assert(F==U.provides(F));
		Assert(F==U.provides(U));
		Assert(F==U.provides(T));

		Assert(F==T.provides(F));
		Assert(T==T.provides(U));
		Assert(T==T.provides(T));
	}
}