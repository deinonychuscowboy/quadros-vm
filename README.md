Quadros VM
==========

This is a virtual machine for a four-state (quaternary) logic system, written in C#. Began as a fun little experiment on my part, and a "let's learn Rust" project, before I realized Rust wasn't the best choice to implement this sort of thing in (Though the rust version did work! It just sucked to actually use.).

All modern digital computers (to the best of my knowledge) are binary, using two values in their logic system. Binary logic has a number of advantages, from being mathematically simple compared to higher valued systems and thus easier to reason about, to being electrically natural to implement.

Back in the annals of computing history, a few prototype machines were made (primarily in Russia) using a three-valued (ternary) logic system, specifically a variant called balanced ternary. This system had a number of advantages; for example, implementing arithmetic circuits is often simpler in balanced ternary than in binary or traditional ternary, thanks to a lower frequency of carry digits in most operations. However, other aspects about balanced ternary were more difficult to use and implement, and so it never really panned out.

At a fundamental level, the advantages of binary and ternary systems are connected to their even vs. odd number of states. When using a given logic system, it is sometimes valuable to express absolutes (one or the other, with no room for alternatives), and sometimes to express comparisons (one, the other, or neither/both/unknown/not applicable). These two behaviors are characteristic of binary and ternary systems, respectively, but exclusive - since binary has no sensible "middle value", it cannot be used for comparisons directly (consider a "between two values" circuit - in binary, there is no way to express the three possible states "higher than both", "between", "lower than both" without two output signals, while in ternary only one would be needed). Likewise, since ternary has an uneven number of values, it cannot sensibly be used for absolutes (the situation is more complex here and is something mathematicians grappled with; Kleene and Priest handled the middle value one way and Łukasiewicz another, equally correct, way. Priest chose two truth values for the purposes of absolutes, Łukasiewicz allowed for absolute statements except in certain cases, and Kleene avoided absolute statements and segregated the middle value).

The value of a four-state system is that it is able to sensibly handle both absolutes and comparisons via conflation. A four-state system can be reduced to two-state evenly via grouping the high and low states, and to a three-state via grouping the middle states. The values in this system are thus characterized as:

- False Known
- Falsey Unknown
- Truthy Unknown
- True Known

such that there are two "Falsey" values, two "Truthy" values, two "Known" values, and two "Unknown" values. Binary-like operations are thus free to care about whether any given value is "Falsey" and "Truthy", and Ternary-like operations may care about whether a given value is "Unknown" or "Unknowable" versus the two distinct "Known" values. These two ways of looking at the values are, in fact, directly translatable from "native" quaternary algebra.

To avoid any sort of associations with names or numeric values used in binary, ternary, or balanced ternary, Quadros names its states with colors: Red, Yellow, Green, and Blue. A single value in Quadros is called a quadrit. Quadrits are not tied intrinsically to any negative/positive state - Binary logic does not handle negation intrinsically, while balanced ternary does, so in Quadros, it may be advantageous in some cases to view Red as meaning "like binary 0", and in other cases to view it as meaning "like balanced ternary -1".

This system is not intended to ever be implemented in a physical circuit. Binary systems are particularly well adapted to electrical signals; while some means could be derived to implement ternary systems (more carefully), four and more values are getting into the range of impracticality and complexity at the electrical engineering level. It is also important not to confuse quadrits with qubits, or quaternary logic with quantum logic; these two systems are completely unrelated and behave very differently.